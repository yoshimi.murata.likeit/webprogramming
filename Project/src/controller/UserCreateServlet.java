package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserCreateServlet
 */
@WebServlet("/UserCreateServlet")
public class UserCreateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserCreateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// <TODO> ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		///ログインユーザ情報をセット
		User XXX=(User)session.getAttribute("userInfo");
		if(XXX==null) {
			//ログインユーザ情報=null（ログインしていない）の場合、ログインサーブレットにリダイレクト
			response.sendRedirect("LoginServlet");
			return;
		}
		// </TODO>

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//getParameter()に
		//formでnameに指定したリクエストパラメータの名前を引数にする
		String createLoginId = request.getParameter("createLoginId");
		String createPassword = request.getParameter("createPassword");
		String createPassword2=request.getParameter("createPassword2");
		String createName=request.getParameter("createName");
		String createBarthDay = request.getParameter("createBarthDay");


		UserDao userDao = new UserDao();
		User XX=userDao.DuplicateCheck(createLoginId);

/*		if(!(createPassword.equals(createPassword2))) {
			//createPasswordとcreatePassword2が一致しない場合の処理
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;
		}else if(!(XX==null)) {
			//すでに登録されているログインIDが入力された場合の処理
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;
		}
*/

		if((!(XX==null))) {
			//すでに登録されているログインIDが入力された場合の処理
			//errMsg出力してuserCreate.jspにフォワード
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);

			return;
		}else if(!(createPassword.equals(createPassword2))) {
			//createPasswordとcreatePassword2が一致しない場合の処理
			//errMsg出力してuserCreate.jspにフォワード
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;
		}else if(createLoginId.isEmpty()||createPassword.isEmpty()||createPassword2.isEmpty()||createName.isEmpty()||createBarthDay.isEmpty()) {
			//入力フォームに未入力の項目があった場合
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
			return;


	}
		//↓登録成功時の処理
				userDao.CreateUser(createLoginId,createPassword,createName,createBarthDay);
				response.sendRedirect("UserListServlet");
}
}




