package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// <TODO> ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		///ログインユーザ情報をセット
		User XXX=(User)session.getAttribute("userInfo");
		if(XXX==null) {
			//ログインユーザ情報=null（ログインしていない）の場合、ログインサーブレットにリダイレクト
			response.sendRedirect("LoginServlet");
			return;
		}
		// </TODO>


		//UserDetailと同じようにidを受け取って、idを引数に、idに紐づく情報を出力
		String id = request.getParameter("id");
		System.out.println(id);
		UserDao userDAO=new UserDao();
		User user =userDAO.findByUSER(id);
		request.setAttribute("user", user);
		//userUpdate.jspで${user.loginId}を表示させる
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
		//フォワード
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO ユーザー編集機能
		//formでnameに指定したリクエストパラメータを取得する
		request.setCharacterEncoding("UTF-8");
		String updatePass=request.getParameter("UpdatePassword");
		String updatePass2=request.getParameter("UpdatePassword2");
		String updateName=request.getParameter("UpdateName");
		String updateBarthDay=request.getParameter("UpdateBarthDay");
		String id = request.getParameter("id");

		if(!(updatePass.equals(updatePass2))) {
			//PasswordとPassword2が一致しなかった場合の処理
			//errMsg出力してuserUpdate.jspにフォワード
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}else if(updatePass.isEmpty() && updatePass2.isEmpty()){
			//パスワード、パスワード確認両方が空欄なら、それ以外を更新する処理

			UserDao userDAO=new UserDao();
			userDAO.UpdateUser2(id,updateName,updateBarthDay);
			response.sendRedirect("UserListServlet");
			//パスワード以外を更新するメソッドにパスワード以外の取得した項目を渡す。
			return;
		}else if(updateName.isEmpty()||updateBarthDay.isEmpty()||id.isEmpty()) {
			//入力フォームにパスワード以外の未入力の項目があった場合
			request.setAttribute("errMsg", "入力された内容は正しくありません");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userCreate.jsp");
			dispatcher.forward(request, response);
		}

		UserDao userDAO=new UserDao();
		//入力項目を引数に渡してUpdateUserメソッドを実行
		userDAO.UpdateUser(id,updatePass,updateName,updateBarthDay);

		response.sendRedirect("UserListServlet");

	}

}

