package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// <TODO> ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		///ログインユーザ情報をセット
		User XXX=(User)session.getAttribute("userInfo");
		if(XXX==null) {
			//ログインユーザ情報=null（ログインしていない）の場合、ログインサーブレットにリダイレクト
			response.sendRedirect("LoginServlet");
			return;
		}
		// </TODO>


		//idを受け取って、idを引数に、idに紐づく情報を出力
		String id = request.getParameter("id");
		System.out.println(id);
		UserDao userDAO=new UserDao();
		User user =userDAO.findByUSER(id);
		request.setAttribute("user", user);
		//userUpdate.jspで${user.loginId}を表示させる
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
		dispatcher.forward(request, response);
		//フォワード
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//削除メソッド
		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");
		UserDao userDAO=new UserDao();
		//User user =userDAO.deleteByUser(id); 間違い。戻り値が無いのに代入できない
		userDAO.deleteByUser(id);
		//引数を渡してメソッドを実行するだけで良い

		response.sendRedirect("UserListServlet");

	}

}
