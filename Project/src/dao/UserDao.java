package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;


/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {


	public String encryption(String encryptionPass) {
		//暗号化メソッド
		//ハッシュを生成したい元の文字列
		String source = encryptionPass;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = null;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);
		return result;

	}

	public void deleteByUser(String id) {
		//メモ：戻り値を返す必要はない
		//UserDereteServlet
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "DELETE FROM user WHERE id=?";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//
	//UserDetail
	public User findByUSER(String id) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE id = ?";
			PreparedStatement pSTMt = conn.prepareStatement(sql);
			pSTMt.setString(1, id);

			ResultSet rS = pSTMt.executeQuery();

			if (!rS.next()) {
				return null;
			}
			int ID = rS.getInt("id");
			String loginId = rS.getString("login_id");
			String name = rS.getString("name");
			Date birthDate = rS.getDate("birth_date");
			Date createDate = rS.getDate("create_date");
			Date updateDate = rS.getDate("update_date");

			return new User(ID, loginId, name, birthDate, createDate, updateDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//UserUpdateServlet
	//idに紐づくlogin_id password name birth_date情報を返す。
	//login_id表示のみ、他は編集可
	//password以外はユーザー情報テーブルから取得して初期値に設定する

	public void UpdateUser(String id, String updatePassword, String updateName, String updateBarthDay) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "UPDATE user SET password=? ,name=? ,birth_date=? WHERE id=?";

			String encryptionPass = encryption(updatePassword);
			//暗号化処理
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, encryptionPass);
			pStmt.setString(2, updateName);
			pStmt.setString(3, updateBarthDay);
			pStmt.setString(4, id);

			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//ログインIDに基づくユーザ情報を返す
	public User findByIdInfo(String loginId) {
		//LoginServlet
		Connection conn = null;
		try {
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id = ? ";
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				return null;
			}
			String loginIdData = rs.getString("login_id");
			return new User(loginIdData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//ID重複を確認するメソッド
	public User DuplicateCheck(String createID) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE login_id = ?";
			//login_id検索してrsに

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, createID);
			ResultSet rs = pStmt.executeQuery();

			if (!rs.next()) {
				//false 重複無
				return null;
			}
			//true 重複あり。インスタンスを返す
			//getString("カラム名")
			String XXX = rs.getString("login_id");
			return new User(XXX);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public User CreateUser(String createID, String createPass, String createName, String createBirthDate) {
		//ユーザ新規作成用
		//UserCreateServlet
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			// INSERT文を準備
			String sql = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date)VALUES(?,?,?,?,NOW(),NOW())";
			//メモ；ここに暗号化の処理を書く
			String encryptionPass = encryption(createPass);

			// INSERTを実行
			PreparedStatement PStmt = conn.prepareStatement(sql);
			PStmt.setString(1, createID);
			PStmt.setString(2, createName);
			PStmt.setString(3, createBirthDate);
			PStmt.setString(4, encryptionPass);
			PStmt.executeUpdate();
			PStmt.close();

		} catch (SQLException e) {
			e.printStackTrace();

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return null;
	}

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByLoginInfo(String loginId, String password) {
		//LoginServlet
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";
			//メモ；ここに暗号化の処理を書く
			String encryptionPass = encryption(password);

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			//pStmt.setString(2, password);
			pStmt.setString(2, encryptionPass);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id!=1";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				Date createDate = rs.getDate("create_date");
				Date updateDate = rs.getDate("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * ユーザー検索
	 * @return
	 */
	public List<User> findResearch(String loginIdP, String nameP, String startDate, String endDate) {
		//ユーザー検索機能
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE id!=1";
			//管理者以外をすべて取得

			//"SELECT * FROM user where login_id=? and name LIKE %?% and (birth_date>=? AND birth_date<=?)";
			//"SELECT * FROM user WHERE id!=1 AND login_id={?(loginIdP)} AND name LIKE %{?(nameP)}% AND (birth_date>=startDate AND birth_date<=endDate)

/*
			if(!(loginIdP.equals(""))) {
				//入力されたログインIDが空欄ではない場合
				//sql += " AND login_id = '" + loginIdP + "'";
				sql += " AND login_id = '" + loginIdP + "AND name LIKE '" + "%'"+ nameP +"%'" + "AND (birth_date>='" + startDate + "AND birth_date<= '" + endDate + ")'" ;
			}else if(loginIdP.equals("")) {
				//ログインIDが空欄の場合
				sql += "AND name LIKE '" + "%'" + nameP + "%'" + "AND (birth_date>='" + startDate + "AND birth_date<= '" + endDate + ")'" ;
			}
*/

			//IFの分岐は4つ
			//それぞれにsql文を加算する
			if(!(loginIdP.isEmpty())) {
				sql += " AND login_id = '" + loginIdP + "'" ;
			}
			if(!(nameP.isEmpty())) {
				sql += " AND name LIKE '" + "%" + nameP + "%'";
			}
			if(!(startDate.isEmpty())) {
				sql+= " AND birth_date>='" + startDate + "'" ;
			}
			if(!(endDate.isEmpty())) {
				sql+= " AND birth_date<= '" + endDate + "'" ;
			}


			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			//PreparedStatement stmt = conn.prepareStatement(sql);
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				Date birthDate2 = rs.getDate("birth_date");
				User user = new User(loginId, name, birthDate,birthDate2);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public void UpdateUser2(String id, String updateName, String updateBarthDay) {
		// TODO 自動生成されたメソッド・スタブ
		//パスワード以外の項目を更新するメソッド
		//UpdateUserServlet

			Connection conn = null;
			try {
				conn = DBManager.getConnection();
				String sql = "UPDATE user SET name=? ,birth_date=? WHERE id=?";
				//String encryptionPass = encryption(updatePassword);
				//パスワードは更新しないので暗号化処理なし
				PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1, updateName);
				pStmt.setString(2, updateBarthDay);
				pStmt.setString(3, id);

				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
}

