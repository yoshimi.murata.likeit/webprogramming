package model;

import java.io.Serializable;
import java.util.Date;


/**
 * Userテーブルのデータを格納するためのBeans
 * @author takano
 *
 */
public class User implements Serializable {
	private int id;
	private String loginId;
	private String name;
	private Date birthDate;
	private String password;
	private Date createDate;
	private Date updateDate;

	// ログインセッションを保存するためのコンストラクタ
	//loginServlet
	public User(String loginId, String name) {
		this.loginId = loginId;
		this.name = name;
	}
	//ID重複メソッドで追加したコンストラクタ
	//UserCreate
	public User(String login_id) {
		this.loginId=login_id;
	}



	// 全てのデータをセットするコンストラクタ
	public User(int id, String loginId, String name, Date birthDate, String password, Date createDate,
			Date updateDate) {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}


	public User(int id, String loginId, String name, Date birthDate, Date createDate, Date updateDate) {
		// UserDetailServlet
		this.id=id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.createDate = createDate;
		this.updateDate = updateDate;
	}



	public User(int id,String updatePassData, String updateNameData, Date updateBirthData) {
		//UserUpdateServlet
		this.id=id;
		this.password=updatePassData;
		this.name=updateNameData;
		this.birthDate=updateBirthData;
	}



	public User(int id) {
		//UserDereteServlet
		this.id=id;
	}



	public User(String loginId2, String name2, Date birthDate2, Date birthDate22) {
		// TODO 自動生成されたコンストラクター・スタブ
		//ユーザー検索機能のコンストラクタ
		this.loginId=loginId2;
		this.name=name2;
		this.birthDate=birthDate2;


	}





	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}



