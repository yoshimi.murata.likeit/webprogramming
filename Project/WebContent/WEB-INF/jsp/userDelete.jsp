<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="index.css" rel="stylesheet" type="text/css" />
</head>
	<body>
        <!--header-->
        <header>
           <nav class="navbar navbar-light bg-light">
                <a class="navbar-text">${userInfo.name} さん</a>
                <form class="form-inline">
				<a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
                </form>
            </nav>
        </header>
        <!--header-->

        <div class="text center">
        <h1>ユーザ削除確認</h1>
        </div>

        <p>ログインID:${user.loginId}を本当に削除してよろしいでしょうか。</p>

		<!-- フォーム -->
		<div>
			<form class="form-signin" action="UserDeleteServlet" method="post">
			<input type="hidden" name="id" value="${user.id}">
          		<button class="btn btn-primary btn-lg" type="submit">削除</button>
          		<!-- submitボタンを押すとUserDeleteServletのPOSTメソッドの内容が実行される -->
            </form>
        </div>

		<div>
			<button type="button" class="btn btn-primary btn-lg" onclick="history.back()">キャンセル</button>
        </div>

		<!-- /フォーム -->
	</body>
</html>

