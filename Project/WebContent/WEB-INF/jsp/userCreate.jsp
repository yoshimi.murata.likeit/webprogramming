<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- EL式を使う場合記述 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="index.css" rel="stylesheet" type="text/css" />
</head>
	<body>
        <!--header-->
        <header>
           <nav class="navbar navbar-light bg-light">
                <a class="navbar-text">${userInfo.name} さん</a>
                <form class="form-inline">
				<a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
                </form>
            </nav>
        </header>
        <!--/header-->

        <div class="text-center">
            <h1>ユーザー新規登録</h1>
        </div>
			<!-- エラーメッセージをEL式で出力 -->
        	<div class="container">
			<c:if test="${errMsg != null}" >
	    		<div class="alert alert-danger" role="alert">
					${errMsg}
				</div>
			</c:if>


		<!-- 入力フォーム -->
		<div>
            <form class="form-signin" action="UserCreateServlet" method="post">
            	<label for="inputCreateId">ログインID</label>
          		<input type="text" name="createLoginId" id="createInputLoginId"
          		class="form-control" placeholder="ログインID" required autofocus>

          		<label for="inputCreatePass">パスワード</label>
          		<input type="text" name="createPassword" id="createInputPassword"
          		class="form-control" placeholder="パスワード" required autofocus>

          		<!-- for,name,id=password>password2に修正。しかしエラー -->
          		<label for="inputCreatePass2">パスワード(確認）</label>
          		<input type="text" name="createPassword2" id="createInputPassword2"
          		class="form-control" placeholder="パスワード（確認）" required autofocus>
          		-->

          		<label for="inputCreateName">ユーザネーム</label>
          		<input type="text" name="createName" id="createInputName"
          		class="form-control" placeholder="ユーザネーム" required autofocus>

          		<label for="inputCreateBarthDay">生年月日</label>
          		<!-- type="text"で生年月日0000/00/00を入力→SQLで0000-00-00が設定され、例外エラーが起きた -->
          		<!-- 参考URL:https://yzn.hatenadiary.org/entry/20061127/p1 -->
          		<!-- input type="date"に修正 -->
          		<input type="date" name="createBarthDay" id="createInputBarthDay"
          		class="form-control" placeholder="生年月日" required autofocus>

          		<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">登録</button>
            </form>
        </div>

		<!-- /入力フォーム -->

        <button type="button" onclick="history.back()">戻る</button>
        <!-- <a href="index.html">戻る</a> -->


	</body>
</html>


