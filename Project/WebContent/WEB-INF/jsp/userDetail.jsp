<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="index.css" rel="stylesheet" type="text/css" />
</head>
	<body>
        <!--header-->
        <header>
           <nav class="navbar navbar-light bg-light">
                <a class="navbar-text">${userInfo.name} さん</a>
                <form class="form-inline">
				<a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
                </form>
            </nav>
        </header>
        <!--header-->

        <div class="text center">
        <h1>ユーザ情報詳細参照</h1>


        </div>

        <div class="form-group row">
            <label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
            <div class="col-sm-10">
                <p class="form-control-plaintext">${user.loginId}</p>
                <p class="form-control-plaintext"></p>
            </div>
        </div>

        <div class="form-group row">
            <label for="name" class="col-sm-2 col-form-label">ユーザ名</label>
            <div class="col-sm-10">

                <p class="form-control-plaintext">${user.name}</p>
            </div>
        </div>

        <div class="form-group row">
            <label for="Bathday" class="col-sm-2 col-form-label">生年月日</label>
            <div class="col-sm-10">
                <p class="form-control-plaintext">${user.birthDate}</p>
            </div>
        </div>

        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">登録日時</label>
            <div class="col-sm-10">
                <p class="form-control-plaintext">${user.createDate}</p>
            </div>
        </div>

        <div class="form-group row">
            <label for="" class="col-sm-2 col-form-label">更新日時</label>
            <div class="col-sm-10">
                <p class="form-control-plaintext">${user.updateDate}</p>
            </div>
        </div>


        <button type="button" onclick="history.back()">戻る</button>
        <!-- <a href="index.html">戻る</a> -->

	</body>
</html>

