<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
	    <title>title</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
    <body>
        <!--header-->
        <header>
           <nav class="navbar navbar-light bg-light">
                <a class="navbar-text">${userInfo.name} さん</a>
                <form class="form-inline">
				<a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
                </form>
            </nav>
        </header>
        <!--header-->

        <div class="text-center">
            <h1>ユーザー一覧</h1>
            <h3>
                <a class="btn btn-outline-success" href="UserCreateServlet">新規登録</a>
            </h3>
        </div>


        <!--検索ボックス-->
        <form class="form-signin" action="UserListServlet" method="post">
        	<div>
            	<label for="inputLoginId">ログインID</label>
            	<input type="text" name="searchLoginId" id="seachLoginId" placeholder="ログインID" class="form-control">

            	<label for="inputName">ユーザー名</label>
            	<input type="text" name="searchName" class="form-control" placeholder="ユーザー名">
        	</div>

        	<div>
           		<label for="inputBarthDay">生年月日</label>
            	<input type="date" name="searchBarthDay" class="form-control" placeholder="Default input">

            	<label for="inputBarthDay2">～</label>
            	<input type="date" name="searchBarthDay2" class="form-control2" placeholder="Default input">
        	</div>


        	<div class="input-group">
	       		<span class="input-group-btn">
		      		<button type="submit" class="btn-outline-success">検索</button>
	       		</span>
        	</div>
        </form>

        <!--検索ボックス-->



        <!--ユーザー表-->

                <div class="table-responsive">
             <table class="table table-striped">
               <thead>
                 <tr>
                   <th>ログインID</th>
                   <th>ユーザ名</th>
                   <th>生年月日</th>
                   <th></th>
                 </tr>
               </thead>
               <tbody>
                 <c:forEach var="user" items="${userList}" >
                 <!-- if(!(user.loginId=='admin')){ -->
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <c:choose>
                     <c:when test="${userInfo.loginId=='admin' }">
                     <!-- loginIdがadmin==管理者ならボタンを3つ表示する -->
                     <td>
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
                     </td>
                     </c:when>

                     <c:otherwise>
                     <!-- 管理者以外なら詳細更新ボタンのみ表示する -->
                     <td>
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       <c:if test="${userInfo.loginId==user.loginId}">
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       </c:if>
                     </td>
                     </c:otherwise>

                     </c:choose>

                   </tr>
                 </c:forEach>
               </tbody>
             </table>
           </div>
        <!--ユーザー表-->



    </body>
</html>