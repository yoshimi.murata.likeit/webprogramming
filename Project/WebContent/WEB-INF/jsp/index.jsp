<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- EL式を使う場合記述 -->
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="index.css" rel="stylesheet" type="text/css" />
</head>

	<body>
<!-- エラーメッセージをEL式で出力 -->
		<div class="container">
			<c:if test="${errMsg != null}" >
	    		<div class="alert alert-danger" role="alert">
					${errMsg}
				</div>
			</c:if>


			<div class="text-center">
             	<h1>ログイン画面</h1>
        	</div>

        <div>
            <form class="form-signin" action="LoginServlet" method="post">
          		<input type="text" name="loginId" id="inputLoginId" class="form-control" placeholder="ログインID" required autofocus>
          		<input type="text" name="password" id="inputPassword" class="form-control" placeholder="パスワード" required autofocus>
          		<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">ログイン</button>
            </form>
        </div>
        </div>

	</body>
</html>


