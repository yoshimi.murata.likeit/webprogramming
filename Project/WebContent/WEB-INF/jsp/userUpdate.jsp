<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>title</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="index.css" rel="stylesheet" type="text/css" />
</head>
	<body>
        <!--header-->
        <header>
           <nav class="navbar navbar-light bg-light">
                <a class="navbar-text">${userInfo.name} さん</a>
                <form class="form-inline">
				<a href="LogoutServlet" class="navbar-link logout-link">ログアウト</a>
                </form>
            </nav>
        </header>
        <!--header-->

        <div class="text-center">
            <h1>ユーザー情報更新</h1>
        </div>
        	<!-- エラーメッセージをEL式で出力 -->
        	<div class="container">
			<c:if test="${errMsg != null}" >
	    		<div class="alert alert-danger" role="alert">
					${errMsg}
				</div>
			</c:if>

        <div>

            	<label for="loginId" class="col-sm-2 col-form-label">ログインID</label>
                <p class="form-control-plaintext">${user.loginId}</p>


        </div>

		<!-- 入力フォーム -->
		<div>
			<form class="form-signin" action="UserUpdateServlet" method="post">
				<!-- nameの内容をリクエストパラメータへ送る -->
				<input type="hidden" name="id" value="${user.id}">

          		<label for="inputUpdatePass">パスワード</label>
          		<input type="text" name="UpdatePassword" id="UpdatePassword"
          		class="form-control" placeholder="パスワード" autofocus>

          		<label for="inputUpdatePass2">パスワード(確認）</label>
          		<input type="text" name="UpdatePassword2" id="UpdatePassword2"
          		class="form-control" placeholder="パスワード（確認）" autofocus>

          		<label for="inputUpdateName">ユーザネーム</label>
          		<input type="text" name="UpdateName" id="UpdateName" value="${user.name}"
          		class="form-control" placeholder="ユーザネーム" required autofocus>

          		<label for="inputUpdateBarthDay">生年月日</label>
          		<input type="date" name="UpdateBarthDay" id="UpdateBarthDay" value="${user.birthDate}"
          		class="form-control" placeholder="生年月日" required autofocus>

          		<button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">更新</button>
          		<!-- submitボタンを押すとUserUpdateServletのPOSTメソッドの内容が実行される -->
            </form>
        </div>

		<!-- /入力フォーム -->

        <button type="button" onclick="history.back()">戻る</button>
        <!-- <a href="index.html">戻る</a> -->


	</body>
</html>

